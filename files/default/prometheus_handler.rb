# The MIT License (MIT)
#
# Copyright © 2017 SoundCloud Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

require "chef/handler"
require "prometheus/client/push"
require 'fileutils'

class PrometheusHandler < Chef::Handler
  attr_reader :textfile, :registry, :labels, :label_values

  JOB = "chef_client".freeze

  def initialize(textfile)
    @textfile = textfile
    @registry = Prometheus::Client.registry

    # Add extra labels from chef node attributes here.
    # example: [:extra_label]
    @labels = []
    # Add the extra label values here.
    # example: { extra_label: 'foo' }
    @label_values = {}
  end

  def report

    # Track the chef client error.
    chef_client_error = 0

    collect_time_metrics()
    collect_role_metrics()
    exception ? chef_client_error = 1 : collect_resource_metrics()
    collect_error_metrics(chef_client_error)
    collect_chef_info_metrics()

    dir = File.dirname(textfile[:textfile])
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    File.write(textfile[:textfile], Prometheus::Client::Formats::Text.marshal(registry))
  rescue => ex
    Chef::Log.error("PrometheusHandler: #{ex.inspect}")
  end

  private

  def collect_time_metrics()
    registry
      .gauge(:chef_client_duration_seconds,
             docstring: "The duration of chef-client run in seconds.",
             labels: labels,
      )
      .set(run_status.elapsed_time, labels: label_values)

    registry
      .gauge(:chef_client_last_run_timestamp_seconds,
             docstring: "The unix timestamp of the finish of the last chef-client run.",
             labels: labels,
       )
      .set(run_status.end_time.to_i, labels: label_values)
  end

  def collect_error_metrics(error_status)
    registry
      .gauge(:chef_client_error,
             docstring: "The bool error status of the last chef-client run.",
             labels: labels,
       )
      .set(error_status, labels: labels)
  end

  def collect_resource_metrics()
    registry
      .gauge(:chef_client_resources,
             docstring: "The number of all resources in the run context.",
             labels: labels,
      )
      .set(run_status.all_resources.size, labels: label_values)

    registry
      .gauge(:chef_client_updated_resources,
             docstring: "The number of all updated resources in the run context.",
             labels: labels,
      )
      .set(run_status.updated_resources.size, labels: label_values)
  end

  def collect_role_metrics()
    roles = registry.gauge(
      :chef_client_roles,
      docstring: "The list of all roles currently applied to the node.",
      labels: [:role] + labels,
    )

    node['roles'].each do |role|
      roles.set(1.0, labels: { role: role }.merge(label_values))
    end
  end

  def collect_chef_info_metrics()
    info_labels = {
      chef_version: Chef::VERSION.to_s,
      ohai_version: Ohai::VERSION.to_s,
    }.merge(label_values)

    registry
      .gauge(:chef_client_info,
             docstring: "The chef client version info.",
             labels: [:chef_version, :ohai_version] + labels,
      )
      .set(1.0, labels: info_labels)
  end
end
