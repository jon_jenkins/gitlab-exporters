require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::elasticsearch_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the prometheus dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/elasticsearch_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the logging ES log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/elasticsearch_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the search ES log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/search_elasticsearch_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'includes the `ark` recipe' do
      expect(chef_run).to include_recipe('ark::default')
    end

    it 'installs elasticsearch_exporter' do
      expect(chef_run).to put_ark('elasticsearch_exporter')
    end

    it 'creates the logging ES runit service' do
      expect(chef_run).to enable_runit_service('elasticsearch_exporter')
    end

    it 'creates the search ES runit service' do
      expect(chef_run).to enable_runit_service('search_elasticsearch_exporter')
    end
  end
end
