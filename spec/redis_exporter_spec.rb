require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::redis_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/redis_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('redis_exporter').with_options(
        hash_including(env: { 'REDIS_PASSWORD' => 'testingpassword' })
      )
    end
  end

  context 'with an alternative vault' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['redis_exporter']['chef_vault'] = 'another-vault'
        node.normal['redis_exporter']['chef_vault_item'] = 'another-item'
      end.converge(described_recipe)
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('redis_exporter').with_options(
        hash_including(env: { 'REDIS_PASSWORD' => 'testingpassword' })
      )
    end
  end

  context 'with user' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['redis_exporter']['env']['REDIS_USER'] = 'redis_exporter'
      end.converge(described_recipe)
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('redis_exporter').with_options(
        hash_including(env: { 'REDIS_USER' => 'redis_exporter', 'REDIS_PASSWORD' => 'testingpassword' })
      )
    end
  end

  context 'with systemd' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['redis_exporter']['systemd'] = true
      end.converge(described_recipe)
    end

    it 'creates the runit service' do
      expect(chef_run).to_not enable_runit_service('redis_exporter')
      expect(chef_run).to_not create_directory('/var/log/prometheus/redis_exporter')

      expect(chef_run).to create_systemd_unit('redis_exporter.service')
      expect(chef_run).to enable_systemd_unit('redis_exporter.service')
      expect(chef_run).to start_systemd_unit('redis_exporter.service')
    end
  end
end
