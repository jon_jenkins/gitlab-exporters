require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::imap_mailbox_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new.converge(described_recipe)
    end

    it 'creates the prometheus dir and plugin in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/imap_mailbox_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/imap_mailbox_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the env directory' do
      expect(chef_run).to create_directory('/etc/imap-mailbox-exporter/env').with(recursive: true)
    end

    %w(username mailbox).each do |env|
      filename = "IMAP_#{env.upcase}"

      it "creates the #{filename} env file" do
        expect(chef_run).to create_file("/etc/imap-mailbox-exporter/env/#{filename}")
          .with(content: chef_run.node['imap_mailbox_exporter'][env])
      end
    end

    it 'creates the IMAP_PASSWORD env file' do
      expect(chef_run).to create_file('/etc/imap-mailbox-exporter/env/IMAP_PASSWORD')
        .with(content: 'hunter1')
    end

    it 'creates the IMAP_SERVER env file' do
      expect(chef_run).to create_file('/etc/imap-mailbox-exporter/env/IMAP_SERVER')
        .with(content: "#{chef_run.node['imap_mailbox_exporter']['server']}:#{chef_run.node['imap_mailbox_exporter']['port']}")
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('imap_mailbox_exporter')
    end
  end
end
