require 'spec_helper'

describe 'gitlab-exporters::gitlab_version_exporter' do
  context 'original version' do
    cached(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'Runs Successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a cron job' do
      expect(chef_run).to create_cron('gitlab version recorder').with(
        command: %q{omnibus_info=$(dpkg-query --showformat='${Version} ${db:Status-Abbrev}' --show gitlab-ee); version=$(echo $omnibus_info | cut -f1 -d ' '); status=$(echo $omnibus_info | cut -f2 -d ' '); echo "omnibus_build_info{version=\"$version\",status=\"$status\"} 1.0" > /opt/prometheus/node_exporter/metrics/omnibus_build_info.prom},
        minute: '*'
      )
    end
  end

  context 'new version' do
    cached(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
      end.converge(described_recipe)
    end

    gve_path = '/opt/gitlab-version-exporter'
    gve_executable = "#{gve_path}/gitlab-version-exporter"
    gve_metric_file = '/opt/prometheus/node_exporter/metrics/gitlab_version_info.prom'

    it 'converges' do
      expect { chef_run }.to_not raise_error
    end

    it 'installs a apt_package with the default action' do
      expect(chef_run).to install_apt_package('jq')
      expect(chef_run).to install_apt_package('moreutils')
    end

    it 'creates a dir' do
      expect(chef_run).to create_directory(gve_path)
    end

    it 'drops in the script' do
      expect(chef_run).to create_cookbook_file(gve_executable)
    end

    it 'creates a cron job' do
      expect(chef_run).to create_cron('gitlab version exporter').with(
        command: "#{gve_executable} | sponge #{gve_metric_file}",
        minute: '*'
      )
    end
  end
end
