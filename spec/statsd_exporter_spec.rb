require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

mapper_content = <<-eos
---
defaults:
  buckets:
  - 0.005
  - 0.01
  - 0.025
  - 0.05
  - 0.1
  - 0.25
  - 0.5
  - 1.0
  - 2.5
  - 10.0
  - 25.0
  match_type: glob
  timer_type: histogram
mappings:
- match: test.*
  name: test_metric_total
  labels:
    test_label: "$1"
eos

describe 'gitlab-exporters::statsd_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['statsd_exporter']['mappings'] = [
          {
            match: 'test.*',
            name: 'test_metric_total',
            labels: {
              test_label: '$1',
            },
          },
        ]
      end.converge('consul::default', described_recipe)
    end

    it 'creates the binary dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/statsd_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/statsd_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'installs packages' do
      expect(chef_run).to install_package('curl')
      expect(chef_run).to install_package('tar')
    end

    it 'installs the statsd_exporter binary' do
      expect(chef_run).to put_ark('statsd_exporter')
    end

    it 'creates a statsd mapping config' do
      expect(chef_run).to create_file('/opt/prometheus/statsd_exporter/statsd_mappings.yml').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/statsd_exporter/statsd_mappings.yml').with_content { |content|
                            expect(content).to eq(mapper_content)
                          }
    end

    it 'creates the systemd service unit' do
      expect(chef_run).to create_systemd_unit('statsd_exporter.service')
      expect(chef_run).to enable_systemd_unit('statsd_exporter.service')
      expect(chef_run).to start_systemd_unit('statsd_exporter.service')
    end
  end
end
