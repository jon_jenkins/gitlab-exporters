require 'yaml'
include_recipe 'gitlab-exporters::default'

directory node['elasticsearch_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['elasticsearch_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['elasticsearch_exporter']['search_log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

# needed, else the DO kitchen tests fail because package 'autogen' can not be found
apt_update 'update' do
end.run_action(:update) if platform_family?('debian')

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

secrets = get_secrets(node['elasticsearch_exporter']['secrets']['backend'],
  node['elasticsearch_exporter']['secrets']['path'],
  node['elasticsearch_exporter']['secrets']['key'])

dir_name = ::File.basename(node['elasticsearch_exporter']['dir'])
dir_path = ::File.dirname(node['elasticsearch_exporter']['dir'])

ark dir_name do
  url node['elasticsearch_exporter']['binary_url']
  checksum node['elasticsearch_exporter']['checksum']
  version node['elasticsearch_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[elasticsearch_exporter]'
  notifies :restart, 'runit_service[search_elasticsearch_exporter]'
end

include_recipe 'runit::default'

# TODO: rename to logging_elasticsearch_exporter. Think about the migration for
# this - perhaps explicitly stop an elasticsearch_exporter service.
runit_service 'elasticsearch_exporter' do
  default_logger true
  log_dir node['elasticsearch_exporter']['log_dir']
  options(
    uri: secrets['elasticsearch_exporter']['es.uri'],
    listen_address: node['elasticsearch_exporter']['listen_address_logging']
  )
  only_if { secrets['elasticsearch_exporter']['es.uri'] }
end

runit_service 'search_elasticsearch_exporter' do
  default_logger true
  log_dir node['elasticsearch_exporter']['search_log_dir']
  run_template_name 'elasticsearch_exporter'
  options(
    uri: secrets['elasticsearch_exporter']['search_es.uri'],
    listen_address: node['elasticsearch_exporter']['listen_address_search']
  )
  only_if { secrets['elasticsearch_exporter']['search_es.uri'] }
end

runit_service 'monitoring_elasticsearch_exporter' do
  default_logger true
  log_dir node['elasticsearch_exporter']['monitoring_log_dir']
  run_template_name 'elasticsearch_exporter'
  options(
    uri: secrets['elasticsearch_exporter']['monitoring_es.uri'],
    listen_address: node['elasticsearch_exporter']['listen_address_monitoring']
  )
  only_if { secrets['elasticsearch_exporter']['monitoring_es.uri'] }
end
