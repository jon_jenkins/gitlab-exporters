#
# Cookbook:: gitlab-exporters
# Recipe:: process_exporter-redis
#
# Copyright:: 2020, GitLab
#

node.default['process_exporter']['config'] = {
  process_names: [
    {
      name: 'redis-sentinel',
      comm: [
        'redis-sentinel',
      ],
    },
    {
      name: 'redis-server',
      comm: [
        'redis-server',
      ],
    },
  ],
}

include_recipe 'gitlab-exporters::process_exporter'
