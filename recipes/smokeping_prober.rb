require 'yaml'
include_recipe 'gitlab-exporters::default'

directory node['smokeping_prober']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['smokeping_prober']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 libcap2-bin ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['smokeping_prober']['dir'])
dir_path = ::File.dirname(node['smokeping_prober']['dir'])

ark dir_name do
  url node['smokeping_prober']['binary_url']
  checksum node['smokeping_prober']['checksum']
  version node['smokeping_prober']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'systemd_unit[smokeping_prober.service]'
end

execute 'setcap smokeping_prober' do
  command "setcap cap_net_raw+ep #{node['smokeping_prober']['binary']}"
  not_if "setcap -v cap_net_raw+ep #{node['smokeping_prober']['binary']}"
end

smokeping_prober_cmd = [
  node['smokeping_prober']['binary'],
  Gitlab::Prometheus.kingpin_flags_for(node, 'smokeping_prober'),
  node['smokeping_prober']['targets'].join(' '),
].join(' ')

smokeping_prober_unit = {
  Unit: {
    Description: 'Prometheus smokeping_prober',
    Documentation: ['https://github.com/SuperQ/smokeping_prober'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: smokeping_prober_cmd,
    KillMode: 'process',
    MemoryLimit: '100M',
    LimitNOFILE: '10000',
    Restart: 'always',
    RestartSec: '5s',
    User: node['prometheus']['user'],
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'smokeping_prober.service' do
  content smokeping_prober_unit
  action [:create, :enable, :start]
end
