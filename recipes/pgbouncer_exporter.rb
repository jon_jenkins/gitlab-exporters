include_recipe 'gitlab-exporters::default'

instances = if node['pgbouncer_exporter']['instances'].empty?
              [node['pgbouncer_exporter'].to_h.slice('log_dir', 'flags')]
            else
              node['pgbouncer_exporter']['instances']
            end

directory node['pgbouncer_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

instances.each do |instance|
  directory instance['log_dir'] do
    owner node['prometheus']['user']
    group node['prometheus']['group']
    mode '0755'
    recursive true
  end
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['pgbouncer_exporter']['dir'])
dir_path = ::File.dirname(node['pgbouncer_exporter']['dir'])

ark dir_name do
  url node['pgbouncer_exporter']['binary_url']
  checksum node['pgbouncer_exporter']['checksum']
  version node['pgbouncer_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put

  instances.each_with_index do |_, index|
    service_name = 'pgbouncer_exporter'
    service_name << "_#{index}" if index > 0
    notifies :restart, "runit_service[#{service_name}]"
  end
end

include_recipe 'runit::default'
instances.each_with_index do |instance, index|
  service_name = 'pgbouncer_exporter'
  service_name << "_#{index}" if index > 0

  runit_service service_name do
    default_logger true
    log_dir instance['log_dir']
    run_template_name 'pgbouncer_exporter'
    options(
      'user' => node['pgbouncer_exporter']['user'],
      'dir' => node['pgbouncer_exporter']['dir'],
      'flags' => Gitlab::Prometheus.kingpin_flags_for({ 'pgbouncer_exporter' => instance }, 'pgbouncer_exporter')
    )
  end
end
