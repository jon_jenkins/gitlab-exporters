# frozen_string_literal: true

# omnibus_build_info{version=\"11.7.5-ee.0\",status=\"ii\"} 1.0
cron 'gitlab version recorder' do
  user 'root'
  minute node['gitlab_version_exporter']['cron_minute']
  command %{omnibus_info=$(dpkg-query --showformat='${Version} ${db:Status-Abbrev}' --show #{node['gitlab_version_exporter']['pkg_name']}); version=$(echo $omnibus_info | cut -f1 -d ' '); status=$(echo $omnibus_info | cut -f2 -d ' '); echo "omnibus_build_info{version=\\\"$version\\\",status=\\\"$status\\\"} 1.0" > #{node['gitlab_version_exporter']['metric_file']}}
end

apt_package 'jq'
apt_package 'moreutils'

directory node['gitlab_version_exporter']['install_path']

gve_executable = "#{node['gitlab_version_exporter']['install_path']}/gitlab-version-exporter"

cookbook_file gve_executable do
  source 'gitlab-version-exporter'
  mode '0770'
end

cron 'gitlab version exporter' do
  user 'root'
  minute node['gitlab_version_exporter']['cron_minute']
  command "#{gve_executable} | sponge #{node['gitlab_version_exporter']['gitlab_metric_file']}"
end
