#
# Cookbook:: gitlab-exporters
# Recipe:: process_exporter-redis-sentinel
#
# Copyright:: 2020, GitLab
#

node.default['process_exporter']['config'] = {
  process_names: [
    {
      name: 'redis-sentinel',
      comm: [
        'redis-sentinel',
      ],
    },
  ],
}

include_recipe 'gitlab-exporters::process_exporter'
