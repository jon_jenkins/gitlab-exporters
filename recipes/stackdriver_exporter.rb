require 'yaml'
include_recipe 'gitlab-exporters::default'

directory node['stackdriver_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['stackdriver_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['stackdriver_exporter']['dir'])
dir_path = ::File.dirname(node['stackdriver_exporter']['dir'])

ark dir_name do
  url node['stackdriver_exporter']['binary_url']
  checksum node['stackdriver_exporter']['checksum']
  version node['stackdriver_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[stackdriver_exporter]'
end

node.default['stackdriver_exporter']['flags']['google.project-id'] = node['gce']['project']['projectId'] unless node['gce'].nil?

include_recipe 'runit::default'
runit_service 'stackdriver_exporter' do
  default_logger true
  log_dir node['stackdriver_exporter']['log_dir']
end
