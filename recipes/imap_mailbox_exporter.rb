include_recipe 'gitlab-exporters::default'

directory node['imap_mailbox_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['imap_mailbox_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 daemontools ).each do |pkg|
  package pkg
end

secrets = get_secrets(node['imap_mailbox_exporter']['secrets']['backend'],
  node['imap_mailbox_exporter']['secrets']['path'],
  node['imap_mailbox_exporter']['secrets']['key'])

dir_basename = ::File.basename(node['imap_mailbox_exporter']['dir'])
dir_dirname = ::File.dirname(node['imap_mailbox_exporter']['dir'])
env_dir = '/etc/imap-mailbox-exporter/env'

directory env_dir do
  recursive true
end

file "#{env_dir}/IMAP_SERVER" do
  content "#{node['imap_mailbox_exporter']['server']}:#{node['imap_mailbox_exporter']['port']}"
end

file "#{env_dir}/IMAP_USERNAME" do
  content node['imap_mailbox_exporter']['username']
end

file "#{env_dir}/IMAP_PASSWORD" do
  content secrets['imap_mailbox_exporter']['password']
end

file "#{env_dir}/IMAP_MAILBOX" do
  content node['imap_mailbox_exporter']['mailbox']
end

ark dir_basename do
  url node['imap_mailbox_exporter']['binary_url']
  checksum node['imap_mailbox_exporter']['checksum']
  version node['imap_mailbox_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_dirname
  owner node['prometheus']['user']
  group node['prometheus']['group']
  extension 'zip'
  action :put
  notifies :restart, 'runit_service[imap_mailbox_exporter]'
end

include_recipe 'runit::default'
runit_service 'imap_mailbox_exporter' do
  default_logger true
  log_dir node['imap_mailbox_exporter']['log_dir']
end
