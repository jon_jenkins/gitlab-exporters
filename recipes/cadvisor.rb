include_recipe 'gitlab-exporters::default'

directory node['cadvisor']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

remote_file node['cadvisor']['binary'] do
  source node['cadvisor']['binary_url']
  checksum node['cadvisor']['sha256sum']
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  action :create
  notifies :restart, 'systemd_unit[cadvisor.service]'
end

cadvisor_cmd = [
  node['cadvisor']['binary'],
  Gitlab::Prometheus.kingpin_flags_for(node, 'cadvisor'),
].join(' ')

cadvisor_unit = {
  Unit: {
    Description: 'cAdvisor Service',
    Documentation: ['https://github.com/google/cadvisor/'],
    Requires: 'basic.target network.target',
  },
  Service: {
    Type: 'simple',
    TimeoutStopSec: '0',
    PrivateDevices: 'yes',
    ProtectSystem: 'full',
    ProtectHome: 'read-only',
    NoNewPrivileges: 'yes',
    ExecStart: cadvisor_cmd,
    KillMode: 'control-group',
    LimitNOFILE: '10000',
    Restart: 'on-failure',
    StartLimitBurst: '3',
    RestartSec: '5s',
    User: 'root',
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'cadvisor.service' do
  content cadvisor_unit
  action [:create, :enable, :start]
  notifies :restart, 'systemd_unit[cadvisor.service]'
end
