#
# Cookbook:: gitlab-exporters
# Recipe:: process_exporter-gitaly
#
# Copyright:: 2020, GitLab
#

node.default['process_exporter']['config'] = {
  process_names: [
    {
      name: 'git-{{.Matches.subcommand}}',
      exe: [
        '/opt/gitlab/embedded/bin/git',
      ],
      cmdline: [
        '^/opt/gitlab/embedded/bin/git(?:\s+(?:-C|-c|--git-dir)\s+\S+)*\s+(?P<subcommand>\w[\w-]+)',
      ],
    },
    {
      name: 'git-{{.Matches.subcommand}}',
      exe: [
        '/opt/gitlab/embedded/libexec/git-core/git',
      ],
      cmdline: [
        '^/opt/gitlab/embedded/libexec/git-core/git(?:\s+(?:-C|-c|--git-dir)\s+\S+)*\s+(?P<subcommand>\w[\w-]+)',
      ],
    },
    {
      name: 'git-{{.Matches.subcommand}}',
      comm: [
        'git',
      ],
      cmdline: [
        '^/[^ ]*/git(?:\s+(?:-C|-c|--git-dir)\s+\S+)*\s+(?P<subcommand>\w[\w-]+)',
      ],
    },
    {
      name: 'gitaly-ruby',
      cmdline: [
        '^ruby\s+/opt/gitlab/embedded/service/gitaly-ruby/bin/gitaly-ruby',
      ],
    },
    {
      name: '{{.ExeBase}}',
      exe: [
        '/opt/gitlab/embedded/bin/gitaly-hooks',
        '/opt/gitlab/embedded/bin/gitaly-lfs-smudge',
        '/opt/gitlab/embedded/bin/gitaly-ssh',
      ],
    },
    {
      name: 'gitaly',
      exe: [
        '/opt/gitlab/embedded/bin/gitaly',
      ],
    },
  ],
}

include_recipe 'gitlab-exporters::process_exporter'
