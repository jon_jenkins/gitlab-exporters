include_recipe 'gitlab-exporters::default'

directory node['consul_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['consul_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['consul_exporter']['dir'])
dir_path = ::File.dirname(node['consul_exporter']['dir'])

ark dir_name do
  url node['consul_exporter']['binary_url']
  checksum node['consul_exporter']['checksum']
  version node['consul_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[consul_exporter]'
end

include_recipe 'runit::default'
runit_service 'consul_exporter' do
  default_logger true
  log_dir node['consul_exporter']['log_dir']
end
