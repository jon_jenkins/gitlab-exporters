default['elasticsearch_exporter']['checksum']       = '1d2444d7cbf321cb31d58d2fecc08c8bc90bbcec581f8a1ddb987f9ef425482b'
default['elasticsearch_exporter']['dir']            = '/opt/prometheus/elasticsearch_exporter'
default['elasticsearch_exporter']['binary']         = "#{node['elasticsearch_exporter']['dir']}/elasticsearch_exporter"
default['elasticsearch_exporter']['log_dir']        = '/var/log/prometheus/elasticsearch_exporter'
default['elasticsearch_exporter']['search_log_dir'] = '/var/log/prometheus/search_elasticsearch_exporter'
default['elasticsearch_exporter']['version']        = '1.1.0'
default['elasticsearch_exporter']['binary_url']     = "https://github.com/justwatchcom/elasticsearch_exporter/releases/download/v#{node['elasticsearch_exporter']['version']}/elasticsearch_exporter-#{node['elasticsearch_exporter']['version']}.linux-amd64.tar.gz"

default['elasticsearch_exporter']['secrets']['backend'] = 'chef_vault'
default['elasticsearch_exporter']['secrets']['path'] = 'elasticsearch_exporter'

# You can add any flags to the runit script by adding another flag attribute
# default["elasticsearch_exporter"]["flags"]["your.config.option"] = "value"

# Elastic instance URI - http://user:pw@xxx.zz:port
# es.uri refers to a logging ES cluster, and search_es.uri refers to a
# repository search cluster.
# This is a hacky way to avoid too many breaking config changes. If we add any
# more clusters we should refactor the config to take a list.
# default["elasticsearch_exporter"]["flags"]["es.uri"] = "in_vault"
# default["elasticsearch_exporter"]["flags"]["search_es.uri"] = "in_vault"

# get metrics for all nodes in the cluster
default['elasticsearch_exporter']['flags']['es.all'] = true

# query stats for cluster settings
# default["elasticsearch_exporter"]["flags"]["es.cluster_settings"] = false

# get stats for all indices
# default["elasticsearch_exporter"]["flags"]["es.indices"] = true

# query settings stats for all indices in the cluster
# default["elasticsearch_exporter"]["flags"]["es.indices_settings"] = false

# get stats for all shards, implies es.indices=true
# default["elasticsearch_exporter"]["flags"]["es.shards"] = true

# get stats for cluster snapshots
# default["elasticsearch_exporter"]["flags"]["es.snapshots"] = true

# timeout for getting stats
default['elasticsearch_exporter']['flags']['es.timeout'] = '20s'

# Address to listen on for web interface and telemetry
default['elasticsearch_exporter']['listen_address_logging'] = '0.0.0.0:9114'
default['elasticsearch_exporter']['listen_address_search'] = '0.0.0.0:9115'
default['elasticsearch_exporter']['listen_address_monitoring'] = '0.0.0.0:9116'
