#
# Cookbook:: GitLab::Monitoring
# Attributes:: imap_mailbox_exporter
#
default['imap_mailbox_exporter']['dir'] = '/opt/prometheus/imap_mailbox_exporter'
default['imap_mailbox_exporter']['log_dir'] = '/var/log/prometheus/imap_mailbox_exporter'
default['imap_mailbox_exporter']['checksum'] = '39ec2c3be0095795aa18019bf4fdcc0fda9f04e893e356dfac4bede37a1dff26'
default['imap_mailbox_exporter']['version'] = '0.2.0'
default['imap_mailbox_exporter']['binary_url'] = "https://ops.gitlab.net/ahmadsherif/imap-mailbox-exporter/-/jobs/artifacts/v#{node['imap_mailbox_exporter']['version']}/download?job=build"

default['imap_mailbox_exporter']['flags']['listen.address'] = '0.0.0.0:9117'

default['imap_mailbox_exporter']['server'] = 'imap.gmail.com'
default['imap_mailbox_exporter']['port'] = '993'
default['imap_mailbox_exporter']['username'] = 'incoming-gstg@gitlab.com'
default['imap_mailbox_exporter']['password'] = 'in vault'
default['imap_mailbox_exporter']['mailbox'] = 'inbox'

default['imap_mailbox_exporter']['secrets']['backend'] = 'chef_vault'
default['imap_mailbox_exporter']['secrets']['path'] = 'imap_mailbox_exporter'
