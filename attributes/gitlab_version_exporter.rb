default['gitlab_version_exporter']['metric_file']      = '/opt/prometheus/node_exporter/metrics/omnibus_build_info.prom'
default['gitlab_version_exporter']['pkg_name']         = 'gitlab-ee'
default['gitlab_version_exporter']['cron_minute']      = '*'

default['gitlab_version_exporter']['install_path'] = '/opt/gitlab-version-exporter'
default['gitlab_version_exporter']['gitlab_metric_file'] = '/opt/prometheus/node_exporter/metrics/gitlab_version_info.prom'
