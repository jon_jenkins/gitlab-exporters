#
# Cookbook:: GitLab::Monitoring
# Attributes:: postgres_exporter
#
default['postgres_exporter']['dir'] = '/opt/prometheus/postgres_exporter'
default['postgres_exporter']['log_dir'] = '/var/log/prometheus/postgres_exporter'
default['postgres_exporter']['checksum'] = 'd2b6b62928f28791b1d0f405776e8740df4b881af1bbd6781dc2c192c55a82c6'
default['postgres_exporter']['version'] = '0.6.0'
default['postgres_exporter']['db_user'] = 'postgres_exporter'
default['postgres_exporter']['database'] = 'gitlabhq_production'
default['postgres_exporter']['binary_url'] = "https://github.com/wrouesnel/postgres_exporter/releases/download/v#{node['postgres_exporter']['version']}/postgres_exporter_v#{node['postgres_exporter']['version']}_linux-amd64.tar.gz"

default['postgres_exporter']['flags']['extend.query-path'] = "#{node['postgres_exporter']['dir']}/queries.yaml"
default['postgres_exporter']['secrets']['backend'] = 'chef_vault'
default['postgres_exporter']['secrets']['path'] = 'postgres-exporter'
