# InSpec tests

control 'groups-and-users' do
  impact 1.0
  title 'General group and user tests'

  describe etc_group do
    its('gids') { should_not contain_duplicates }
  end

  describe user('prometheus') do
    it { should exist }
  end
end

control 'elasticsearch_exporter is properly installed' do
  impact 1.0

  describe file('/opt/prometheus/elasticsearch_exporter/elasticsearch_exporter') do
    it { should exist }
  end

  describe file('/etc/sv/elasticsearch_exporter/run') do
    it { should exist }
    its('content') { should include('--es.uri=https://blah:blub@elasticioicluster.io:9200') }
  end

  describe file('/etc/sv/search_elasticsearch_exporter/run') do
    it { should exist }
    its('content') { should include('https://search:searchpass@clusters_r_us.com:9200') }
  end

  describe runit_service('elasticsearch_exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe runit_service('search_elasticsearch_exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
