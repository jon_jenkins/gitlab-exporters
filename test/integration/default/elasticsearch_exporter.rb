# InSpec tests

control 'elasticsearch_exporter is properly installed' do
  impact 1.0

  describe file('/opt/prometheus/elasticsearch_exporter/elasticsearch_exporter') do
    it { should exist }
  end

  describe file('/etc/sv/elasticsearch_exporter/run') do
    it { should exist }
    its('content') { should include('--es.uri=https://blah:blub@elasticioicluster.io:9200') }
  end

  describe runit_service('elasticsearch_exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
