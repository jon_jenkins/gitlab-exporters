# frozen_string_literal: true

control 'Installs gitlab-version-exporter' do
  describe package('jq') do
    it { should be_installed }
  end

  describe package('moreutils') do
    it { should be_installed }
  end

  describe command('jq') do
    it { should exist }
  end

  describe command('sponge') do
    it { should exist }
  end

  gve_executable = '/opt/gitlab-version-exporter/gitlab-version-exporter'
  gve_metrics_path = '/opt/prometheus/node_exporter/metrics/gitlab_version_info.prom'

  describe file(gve_executable) do
    it { should exist }
    it { should be_allowed('execute') }
  end

  describe crontab('root') do
    its('commands') { should include "#{gve_executable} | sponge #{gve_metrics_path}" }
  end
end
